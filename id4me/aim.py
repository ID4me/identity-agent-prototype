import hashlib
import json

from encodings import idna
from dns.exception import Timeout
from dns.resolver import Resolver, NXDOMAIN, YXDOMAIN, NoAnswer, NoNameservers
from publicsuffix import PublicSuffixList

import jwt

from id4me import app
from id4me.identity_authority import get_identity_authority, Unauthorized
from id4me.models import Registration, IdentityAuthority, NotRegisteredError
from id4me.network import get_jws_detached_data, post_jws_detached_data


resolver = Resolver()
if 'DNS_NAMESERVERS' in app.config:
    resolver.nameservers = app.config['DNS_NAMESERVERS'].split(',')

psl = PublicSuffixList()


def identify_domain_authority(domain):
    return psl.get_public_suffix(domain)


def convert_domain_to_ascii(domain):
    authority = identify_domain_authority(domain)
    if domain == authority:
        return idna.ToASCII(domain).decode()
    return idna.ToASCII(domain.replace('.' + authority, '')).decode() + "." + idna.ToASCII(authority).decode()


def get_acme_configuration():
    return {
        'newAuthz': '{}/authz'.format(app.config.get("IAU_AIM")),
        'newAccount': '{}/agents'.format(app.config.get("IAU_AIM")),
        'account': '{}/identities/{{}}'.format(app.config.get("IAU_AIM"))
    }


def get_jwk():
    return {
        'e': app.public_key_e.decode(),
        'kty': 'RSA',
        'n': app.public_key_n.decode().strip("=")
    }


def create_agent_account():
    config = get_acme_configuration()
    jwk = get_jwk()
    jwk['use'] = 'sig'

    headers = {
        'kid': '',
        'method': 'POST',
        'b64': False,
        'jwk': jwk,
        'url': config['newAccount'],
        'use': 'sig'
    }
    payload = {
        'name': app.config.get("IAU_ACCOUNT_NAME"),
        'fullName': app.config.get("IAU_ACCOUNT_FULL_NAME"),
        'email': app.config.get("IAU_ACCOUNT_EMAIL"),
    }

    response = post_jws_detached_data(url=config['newAccount'], headers=headers, payload=payload)
    IdentityAuthority.update_identity_authority_agent_config(app.config.get("IAU"), json.dumps(response))


def check_iau_account():
    try:
        IdentityAuthority.get_local_identity_authority_agent_config(app.config.get("IAU"))
    except NotRegisteredError:
        create_agent_account()


def get_fingerprint():
    jwk = json.dumps(get_jwk(), sort_keys=True).replace(", ", ",").replace(": ", ":")
    return jwt.utils.base64url_encode(hashlib.sha256(jwk.encode()).digest()).decode()


def get_iau_kid():
    config = IdentityAuthority.get_local_identity_authority_agent_config(app.config.get('IAU'))
    return config['id']


def check_existing_registration(domain):
    registration = Registration.get_domain_registration(domain)
    return registration is not None


def check_existing_challenge(domain):
    registration = Registration.get_domain_registration(domain)
    return registration.token is not None


def check_dns_for_existing_acme_record(domain):
    hostname = '_acme-challenge.{}'.format(domain)
    try:
        dns = resolver.query(hostname, 'TXT')
    except Timeout:
        print('Timeout. Failed to resolve "{}"'.format(hostname))
        return False
    except NXDOMAIN or YXDOMAIN:
        print('Failed to resolve "{}"'.format(hostname))
        return False
    except NoAnswer:
        print('Failed to find ACME records for "{}"'.format(hostname))
        return False
    except NoNameservers:
        print('No nameservers avalaible to dig "{}"'.format(hostname))
        return False
    return True


def get_openid_record():
    return {
        'name': '_openid',
        'type': 'TXT',
        'value': 'v=OID1;iss={};clp={}'.format(app.config['IAU'], app.config['SERVER_URL'].replace('https://', ''))
    }


def get_dns_challenge_record(domain):
    registration = Registration.get_domain_registration(domain)
    message = registration.token + "." + get_fingerprint()
    return {
        'name': '_acme-challenge',
        'type': 'TXT',
        'value': jwt.utils.base64url_encode(hashlib.sha256(message.encode()).digest()).decode()
    }


def request_dns_challenge(domain, email):
    check_iau_account()
    config = get_acme_configuration()
    headers = {
        'kid': get_iau_kid(),
        'method': 'POST',
        'url': config['newAuthz'],
        'use': 'sig'
    }
    payload = {
        'identifier': convert_domain_to_ascii(domain),
        'locale': 'en'
    }
    response = post_jws_detached_data(url=config['newAuthz'], payload=payload, headers=headers)
    try:
        challenge = response['challenge']
    except KeyError:
        raise Unauthorized('Cannot start challenge. Response from authority: {}'.format(response))
    Registration.create_registration(domain, email)
    Registration.new_challenge(domain, challenge['token'], challenge['url'])


def confirm_dns_challenge(domain):
    check_iau_account()
    try:
        get_identity_authority(domain)
    except Unauthorized:
        print("No identity authority found for {}".format(domain))
        raise Unauthorized
    config = get_acme_configuration()
    registration = Registration.get_domain_registration(domain)
    headers = {
        'kid': get_iau_kid(),
        'method': 'POST',
        'url': registration.challenge_url,
        'use': 'sig'
    }
    result = post_jws_detached_data(url=registration.challenge_url, headers=headers, payload={}, response_json=False)
    if result.status != 200:
        # raise unauthorized if not valid attempt
        error = json.loads(result.read().decode('utf-8'))['message']
        if error == 'The requested ACME challenge is expired. Please acquire a new one.':
            Registration.finalize_challenge(domain)
            return None, None
        raise Unauthorized

    headers = {
        'kid': get_iau_kid(),
        'method': 'GET',
        'url': config.get('account').format(domain),
        'use': 'sig'
    }
    response = get_jws_detached_data(url=config.get('account').format(domain), headers=headers)
    Registration.complete_registration(domain)
    magic_link = response.get('url')
    return registration.email, magic_link
