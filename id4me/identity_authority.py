import binascii
import datetime
import json
import jwt

from dns.exception import Timeout
from dns.resolver import Resolver, NXDOMAIN, YXDOMAIN, NoAnswer, NoNameservers
from dns.message import make_query
import dns.name
import dns.dnssec

import jwt
from Crypto.PublicKey import RSA
from flask import url_for
from werkzeug.exceptions import Unauthorized

from id4me import app
from id4me.models import IdentityAuthority
from id4me.network import get_json, get_json_auth, post_json

from Crypto.PublicKey import RSA

resolver = Resolver()
if 'DNS_NAMESERVERS' in app.config:
    resolver.nameservers = app.config['DNS_NAMESERVERS'].split(',')


class Utc(datetime.tzinfo):
    def utcoffset(self, dt):
        return datetime.timedelta(0)

    def dst(self, dt):
        return datetime.timedelta(0)

    def tzname(self, dt):
        return 'UTC'


UTC = Utc()


def check_dns_sec(domain):
    try:
        domain_authority = resolver.query(domain, 'SOA')
        response = resolver.query(domain_authority, 'NS')
        nsname = response.rrset[0]
        response = resolver.query(nsname, 'A')
        nsaddr = response.rrset[0].to_text()
        request = make_query(domain, 'DNSKEY', want_dnssec=True)
        response = resolver.query.udp(request,nsaddr)
        if response.rcode() != 0:
            print('No DNSKEY record found for {}'.format(domain))
            # raise Unauthorized('No DNSKEY record found for {}'.format(domain))
        else:
            answer = response.answer
            if len(answer) != 2:
                print('DNSSEC check failed for {}'.format(domain))
                # raise Unauthorized('DNSSEC check failed for {}'.format(domain))
            else:
                name = dns.name.from_text(hostname)
                try:
                    dns.dnssec.validate(answer[0], answer[1], {name:answer[0]})
                    print('DNS response for "{}" is signed.'.format(domain))
                except dns.dnssec.ValidationFailure:
                    print('DNS response for "{}" is insecure. Trusting it anyway'.format(domain))
                    # raise Unauthorized('DNS response for "{}" is insecure. Trusting it anyway'.format(hostname))
    except Exception:
        print('DNSSEC check failed for {}'.format(domain))
        # raise Unauthorized('DNSSEC check failed for {}'.format(domain))


def get_identity_authority(domain):
    hostname = '_openid.{}.'.format(domain)
    print('Resolving "{}"'.format(hostname))
    try:
        dns = resolver.query(hostname, 'TXT')
        # enforce strict DNSSEC policy here
        check_dns_sec(domain)
        for txt in dns:
            value = str(txt).replace('"', '')
            print('Checking TXT record "{}"'.format(value))
            if not value.startswith('v=OID1;'):
                continue
            for item in value.split(';'):
                if item.startswith('iau=') or item.startswith('iss='):
                    return item[4:]
    except Timeout:
        print('Timeout. Failed to resolve "{}"'.format(hostname))
        raise Unauthorized('Timeout. Failed to resolve "{}"'.format(hostname))
    except NXDOMAIN or YXDOMAIN:
        print('Failed to resolve "{}"'.format(hostname))
        raise Unauthorized('Failed to resolve "{}"'.format(hostname))
    except NoAnswer:
        print('Failed to find TXT records for "{}"'.format(hostname))
        raise Unauthorized('Failed to find TXT records for "{}"'.format(hostname))
    except NoNameservers:
        print('No nameservers avalaible to dig "{}"'.format(hostname))
        raise Unauthorized('No nameservers avalaible to dig "{}"'.format(hostname))
    print('No suitable TXT DNS entry found for {}'.format(domain))
    raise Unauthorized('No suitable TXT DNS entry found for {}'.format(domain))


def get_identity_authority_configuration(identity_authority):
    try:
        url = '{}/.well-known/openid-configuration'.format(identity_authority)
        if not url.startswith('https'):
            url = 'https://{}'.format(url)
        return get_json_auth(url)
    except Exception as e:
        print(e)
        raise Unauthorized('Could not get configuration for {}'.format(identity_authority))


def get_identity_authority_public_keys(url, kid=None):
    if url.endswith('/'):
        url = '{}.well-known/openid-configuration'.format(url)
    else:
        url = '{}/.well-known/openid-configuration'.format(url)
    try:
        open_id_conf = get_json(url)
    except Exception:
        raise Unauthorized('Could not get configuration for {}'.format(url))
    try:
        jwks = get_json(open_id_conf['jwks_uri'])
    except Exception:
        raise Unauthorized('Could not get configuration for {}'.format(url))
    result = []
    for key in jwks['keys']:
        try:
            if kid is not None and kid != key['kid']:
                continue
        except KeyError:
            pass
        if key['kty'] == 'RSA':
            e = int(binascii.hexlify(jwt.utils.base64url_decode(str(key['e']).encode())), 16)
            n = int(binascii.hexlify(jwt.utils.base64url_decode(str(key['n']).encode())), 16)
            pubkey = RSA.construct((n, e)).publickey()
            result.append(pubkey.exportKey())
    return result


def register_identity_authority(identity_authority, identity_authority_config):
    print('registering with new identity authority ({})'.format(identity_authority))
    request = {
        'redirect_uris': ['{}{}'.format(app.config['SERVER_URL'], url_for('validate'))],
        'client_name': app.config.get("IAU_ACCOUNT_FULL_NAME"),
        'jwks_uri': '{}{}'.format(app.config['SERVER_URL'], url_for('jwks'))
    }
    try:
        config = post_json(identity_authority_config['registration_endpoint'], request)
    except Exception:
        raise Unauthorized('Could not register {}'.format(identity_authority))
    IdentityAuthority.update_identity_authority_client_config(identity_authority, config)
    return json.loads(config)


def require_openid_delegation(subject, issuer):
    if subject.endswith('.test'):
        print('Skipping dns delegation check for "%s" because of test domain', subject)
        return
    print('Checking if "%s" is allowed to authorize requests for "%s"', issuer,
          subject)
    if issuer.startswith('https://'):
        print('Ignoring protocol prefix of issuer')
        issuer = issuer[8:]
    elif issuer.startswith('http://'):
        print('Ignoring protocol prefix of issuer')
        issuer = issuer[7:]
    hostname = '_openid.{}.'.format(subject)
    print('Resolving "{}"'.format(hostname))
    try:
        dns = resolver.query(hostname, 'TXT')
        # enforce strict DNSSEC policy here
        check_dns_sec(hostname)
        for txt in dns:
            value = str(txt).replace('"', '')
            print('Checking TXT record "{}"'.format(value))
            if not value.startswith('v=OID1;'):
                continue
            for item in value.split(';'):
                if item.startswith('iau=') or item.startswith('iss='):
                    authority = item[4:]
                    if authority == issuer:
                        print('TXT record matches')
                        return
                    print('TXT record does not match')
                    break
    except Timeout:
        print('Timeout. Failed to resolve "{}"'.format(hostname))
        raise Unauthorized('Timeout. Failed to resolve "{}"'.format(hostname))
    except NXDOMAIN or YXDOMAIN:
        print('Failed to resolve "{}"'.format(hostname))
        raise Unauthorized('Failed to resolve "{}"'.format(hostname))
    except NoAnswer:
        print('Failed to find TXT records for "{}"'.format(hostname))
        raise Unauthorized('Failed to find TXT records for "{}"'.format(hostname))
    except NoNameservers:
        print('No nameservers avalaible to dig "{}"'.format(hostname))
        raise Unauthorized('No nameservers avalaible to dig "{}"'.format(hostname))
    print('No suitable TXT DNS entry found for {}'.format(domain))
    raise Unauthorized('No suitable TXT DNS entry found for {}'.format(domain))


def decode_validation_token(token, is_id_token=True):
    try:
        payload = jwt.decode(token, verify=False)
        token_header = jwt.get_unverified_header(token)
    except jwt.InvalidTokenError as ex:
        print('Token validation failed: {}'.format(ex))
        raise Unauthorized(str(ex))
    try:
        issuer = payload['iss']
    except KeyError:
        print('Issuer missing in token (iss)')
        raise Unauthorized('Issuer missing in token (iss)')

    if is_id_token:
        options = {'require_exp': True, 'require_iat': True, 'verify_aud': False}
    else:
        options = {'require_exp': False, 'require_iat': False, 'verify_aud': False}
    print('issuer = "{}"'.format(issuer))
    keys = get_identity_authority_public_keys(issuer, token_header.get('kid'))
    try:
        for key in keys:
            try:
                return jwt.decode(token, key, options=options,
                                  leeway=datetime.timedelta(minutes=5))
            except jwt.DecodeError as ex:
                if str(ex) == 'Signature verification failed':
                    pass
                else:
                    raise ex
    except jwt.InvalidTokenError as ex:
        print('Token validation failed: {}'.format(ex))
        raise Unauthorized(str(ex))
    print('Failed to find suitable key to validate token')
    raise Unauthorized('Failed to find suitable key to validate token')


def decode_access_token(token, issuer, client_id, domain):
    try:
        payload = jwt.decode(token, verify=False)
        token_header = jwt.get_unverified_header(token)
    except jwt.InvalidTokenError as ex:
        print('Token validation failed: {}'.format(ex))
        raise Unauthorized(str(ex))
    try:
        if 'id4me.identifier' in payload and domain != payload['id4me.identifier']:
            print('Id4me mismatch in token')
            raise Unauthorized('Issuer mismatch in token')
        if issuer != payload['iss']:
            print('Issuer mismatch in token')
            raise Unauthorized('Issuer mismatch in token')
        if client_id != payload['client_id']:
            print('Client id mismatch in token')
            raise Unauthorized('Client id mismatch in token')
    except KeyError as ex:
        msg = 'Access token missing field {}'.format(ex)
        print(msg)
        raise Unauthorized(msg)

    options = {'require_exp': True, 'require_iat': True, 'verify_aud': False}
    leeway = datetime.timedelta(minutes=5)
    keys = get_identity_authority_public_keys(issuer, token_header.get('kid'))
    try:
        for key in keys:
            try:
                return jwt.decode(token, key, options=options, leeway=leeway)
            except jwt.DecodeError as ex:
                if str(ex) == 'Signature verification failed':
                    pass
                else:
                    raise ex
    except jwt.InvalidTokenError as ex:
        print('Token validation failed: {}'.format(ex))
        raise Unauthorized(str(ex))
    print('Failed to find suitable key to validate token')
    raise Unauthorized('Failed to find suitable key to validate token')
