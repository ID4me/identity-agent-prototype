import base64
import json
import re
import ssl


from http import client

import jwt
from jwt import PyJWS
from werkzeug.exceptions import BadRequest

from id4me import app


def http_request(method, url, body=None, basic_auth=None, bearer=None, content_type=None, cache_control=None, signature=None):
    url_parts = re.match('(?i)(https?)://([^:/]+(?::\d+)?)(/.*)', url)
    if url_parts is None:
        raise Exception('Given issuer is not a valid URL')
    protocol = url_parts.group(1).lower()
    host = url_parts.group(2)
    path = url_parts.group(3)
    print('method = {} protocol = {}, host = {}, path = {}'.format(method, protocol, host, path))
    if protocol == 'http':
        if 'PROXY_HOST' in app.config and 'PROXY_PORT' in app.config:
            print('using proxy {}:{}'.format(app.config['PROXY_HOST'], app.config['PROXY_PORT']))
            connection = client.HTTPConnection(app.config['PROXY_HOST'], app.config['PROXY_PORT'])
            connection.set_tunnel(host)
        else:
            connection = client.HTTPConnection(host)
    else:
        if 'PROXY_HOST' in app.config and 'PROXY_PORT' in app.config:
            print('using proxy {}:{}'.format(app.config['PROXY_HOST'], app.config['PROXY_PORT']))
            connection = client.HTTPSConnection(app.config['PROXY_HOST'], app.config['PROXY_PORT'])
            connection.set_tunnel(host)
        else:
            connection = client.HTTPSConnection(host)
    header = dict()
    if basic_auth is not None:
        user, password = basic_auth
        head = ':'.join([user, password]).encode()
        header['Authorization'] = ' '.join(['Basic', base64.b64encode(head).decode()])
    if bearer is not None:
        header['Authorization'] = ' '.join(['Bearer', bearer])
    if content_type is not None:
        header['Content-Type'] = content_type
    if cache_control is not None:
        header['Cache-Control'] = cache_control
    if signature is not None:
        header['jws-detached-signature'] = signature
    connection.request(method, path, body, header)
    return connection.getresponse()


def post_data(url, data, basic_auth=None, bearer=None):
    response = http_request('POST', url, body=data, basic_auth=basic_auth,
                            bearer=bearer, content_type='application/x-www-form-urlencoded')
    if response.status != 200:
        print(response.getheaders())
        raise Exception('Failed to POST data to {}'.format(url))
    return response.read().decode('utf-8')


def post_json(url, content, basic_auth=None, bearer=None):
    response = http_request('POST', url, body=json.dumps(content), basic_auth=basic_auth,
                            bearer=bearer, content_type='application/json')
    if response.status != 200:
        print(response.getheaders())
        raise Exception('Failed to POST json to {}'.format(url))
    return response.read().decode('utf-8')


def get_json(url):
    url_parts = re.match('(?i)(https?)://([^:/]+(?::\d+)?)(/.*)', url)
    if url_parts is None:
        raise BadRequest('Given issuer is not a valid URL')
    protocol = url_parts.group(1).lower()
    host = url_parts.group(2)
    path = url_parts.group(3)
    print('protocol = {}, host = {}, path = {}'.format(protocol, host, path))
    if protocol == 'http':
        if 'PROXY_HOST' in app.config and 'PROXY_PORT' in app.config:
            print('using proxy {}:{}'.format(app.config['PROXY_HOST'], app.config['PROXY_PORT']))
            connection = client.HTTPConnection(app.config['PROXY_HOST'], app.config['PROXY_PORT'])
            connection.set_tunnel(host)
        else:
            connection = client.HTTPConnection(host)
    else:
        context = ssl._create_unverified_context()
        if 'PROXY_HOST' in app.config and 'PROXY_PORT' in app.config:
            print('using proxy {}:{}'.format(app.config['PROXY_HOST'], app.config['PROXY_PORT']))
            connection = client.HTTPSConnection(app.config['PROXY_HOST'], app.config['PROXY_PORT'], context=context)
            connection.set_tunnel(host)
        else:
            connection = client.HTTPSConnection(host, context=context)
    connection.request('GET', path)
    response = connection.getresponse()
    if response.status != 200:
        print('Failed to query {}: {}'.format(url, response.status))
        raise Exception('Failed to read configuration from {}'.format(url))
    return json.loads(response.read().decode('utf-8'))


def get_json_auth(url, basic_auth=None, bearer=None):
    response = http_request('GET', url, basic_auth=basic_auth, bearer=bearer)
    if response.status != 200:
        print(response.getheaders())
        raise Exception('Failed to GET from {}'.format(url))
    return json.loads(response.read().decode('utf-8'))


def get_jws_detached_data(url, headers):
    token = PyJWS().encode(payload=b'', key=app.private_key, algorithm='RS256', headers=headers)
    signature = re.sub("\\..*?\\.", "..", token.decode('utf-8'))
    response = http_request('GET', url, content_type='application/json', cache_control='no-cache',
                            signature=signature)
    if response.status >= 400:
        raise Exception('Failed to GET {} {} - {}'.format(url, str(response.status), response.read().decode()))
    return json.loads(response.read().decode('utf-8'))


def post_jws_detached_data(url, payload, headers, response_json=True):
    token = jwt.encode(payload=payload, key=app.private_key, algorithm='RS256', headers=headers)
    signature = re.sub("\\..*?\\.", "..", token.decode('utf-8'))
    body = json.dumps(payload, separators=(',', ':'),cls=None).encode('utf-8')
    response = http_request('POST', url, body=body, content_type='application/json', cache_control='no-cache',
                            signature=signature)
    if not response_json:
        return response
    return json.loads(response.read().decode('utf-8'))
