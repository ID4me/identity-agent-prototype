import binascii

import jwt
from Crypto.PublicKey import RSA
from flask import Flask

app = Flask(__name__)
app.config.from_envvar('ID4ME_CONFIG')
app.secret_key = app.config['SESSION_SECRET']
with open(app.config['PRIVATE_KEY'], 'r') as f:
    app.private_key = f.read()


def num_to_bytes(n):
    s = hex(n)[2:]
    s = s.rstrip('L')
    if len(s) & 1:
        s = '0' + s
    return binascii.unhexlify(s)


rsa = RSA.importKey(app.private_key)
app.public_key_e = jwt.utils.base64url_encode(num_to_bytes(rsa.e))
app.public_key_n = jwt.utils.base64url_encode(num_to_bytes(rsa.n))

from id4me.views import *
