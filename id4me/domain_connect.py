import urllib

from encodings import idna
from dns.exception import Timeout
from dns.resolver import Resolver, NXDOMAIN, YXDOMAIN, NoAnswer, NoNameservers

from flask import url_for

from id4me import app
from id4me.aim import get_openid_record, get_dns_challenge_record, identify_domain_authority
from id4me.models import Registration
from id4me.network import get_json


resolver = Resolver()
if 'DNS_NAMESERVERS' in app.config:
    resolver.nameservers = app.config['DNS_NAMESERVERS'].split(',')


def identify_domain_connect(domain, authority):
    hostname = '_domainconnect.{}'.format(authority)
    try:
        dns = resolver.query(hostname, 'CNAME')
        domain_connect = str(dns[0])
        print('Domain Connect {} for {} found.'.format(domain_connect, domain))
        return domain_connect, None
    except Timeout:
        print('Timeout. Failed to find Domain Connect for "{}"'.format(hostname))
        return None, 'Timeout. Failed to find Domain Connect for "{}"'.format(domain)
    except NXDOMAIN or YXDOMAIN:
        print('Failed to resolve "{}"'.format(hostname))
        return None, 'Failed to resolve "{}"'.format(domain)
    except NoAnswer:
        print('No Domain Connect found for "{}"'.format(hostname))
        return None, 'No Domain Connect found for "{}"'.format(domain)
    except NoNameservers:
        print('No nameservers avalaible to dig "{}"'.format(hostname))
        return None, 'No nameservers avalaible to dig "{}"'.format(domain)
    except Exception:
        pass
    print('No Domain Connect found for "{}"'.format(hostname))
    return None, 'No Domain Connect found for "{}"'.format(domain)


def identify_domain_connect_api(domain, domain_connect):
    try:
        dns = resolver.query(domain_connect, 'TXT')
        domain_connect_api = str(dns[0]).replace('"', '')
        print('Domain Connect API {} for {} found.'.format(domain_connect_api, domain))
        return domain_connect_api, None
    except Timeout:
        print('Timeout. Failed to find Domain Connect API for "{}"'.format(domain_connect))
        return None, 'Timeout. Failed to find Domain Connect API for "{}"'.format(domain)
    except NXDOMAIN or YXDOMAIN:
        print('Failed to resolve "{}"'.format(domain_connect))
        return None, 'Failed to resolve "{}"'.format(domain)
    except NoAnswer:
        print('No Domain Connect API found for "{}"'.format(domain_connect))
        return None, 'No Domain Connect API found for "{}"'.format(domain)
    except NoNameservers:
        print('No nameservers avalaible to dig "{}"'.format(domain_connect))
        return None, 'No nameservers avalaible to dig "{}"'.format(domain)
    except Exception:
        pass
    print('No Domain Connect API found for "{}"'.format(domain_connect))
    return None, 'No Domain Connect API found for "{}"'.format(domain)


def identify_template_sync_url(authority, domain_connect_api):
    url = 'https://{}/v2/{}/settings'.format(domain_connect_api, authority)
    try:
        response = get_json(url)
        if 'urlSyncUX' in response:
            print('Domain Connect templateSync API {} for {} found.'.format(domain_connect_api, response['urlSyncUX']))
            return response['urlSyncUX'], None
    except Exception:
        pass
    print('No Domain Connect templateSync API found for {}.'.format(authority))
    return None, 'No Domain Connect templateSync API found for {}.'.format(authority)


def get_domain_connect_template_redirect_url(domain):
    registration = Registration.get_domain_registration(domain)
    if not registration:
        return None, 'No ACME challenge started yet.'

    authority = identify_domain_authority(domain)

    domain_connect, error = identify_domain_connect(domain, authority)
    if error:
        return None, error

    domain_connect_api, error = identify_domain_connect_api(domain, domain_connect)
    if error:
        return None, error

    template_url, error = identify_template_sync_url(authority, domain_connect_api)
    if error:
        return None, error

    sync_url_format = '{}/v2/domainTemplates/providers/{}/services/activation/' \
                      'apply?domain={}&OPENID={}&ACME={}&groupId=acme,openid'

    server = app.config['DOMAIN_CONNECT_PROVIDER']
    if domain != authority:
        sync_url_format += '&host=' + idna.ToASCII(domain.replace('.' + authority, '')).decode()
    openid_record = urllib.parse.quote(get_openid_record()['value'])
    acme_record = urllib.parse.quote(get_dns_challenge_record(domain)['value'])

    return sync_url_format.format(template_url, server, idna.ToASCII(authority).decode(), openid_record, acme_record), None
