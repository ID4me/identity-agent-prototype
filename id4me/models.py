import datetime
import json
import locale
import re
import time
import pytz

from flask_sqlalchemy import SQLAlchemy

from id4me import app

db = SQLAlchemy(app)


class User(db.Model):
    SCOPE_CLAIMS = {
        'profile': [
            'name', 'family_name', 'given_name', 'middle_name', 'nickname', 'preferred_username', 'profile', 'picture',
            'website', 'gender', 'birthdate', 'zoneinfo', 'locale'
        ],
        'email': [
            'email', 'email_verified'
        ],
        'phone': [
            'phone_number', 'phone_number_verified'
        ]
    }

    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    authority = db.Column(db.Text, nullable=False)
    sub = db.Column(db.Text, nullable=False)
    claims = db.Column(db.Text, nullable=False, default='{}')
    updated_at = db.Column(db.DateTime(timezone=True))
    domains = db.relationship('Domain')

    def get_dict(self):
        return {
            'id': self.id,
            'authority': self.authority,
            'sub': self.sub
        }
    
    @classmethod
    def register_domain(cls, authority, sub, domain):
        user = cls.get_user(authority, sub)
        if not user:
            return
        exists = db.session.query(Domain).filter(Domain.domain == domain).count() > 0
        if exists:
            entity = db.session.query(Domain).filter(Domain.domain == domain).first()
        else:
            entity = Domain(domain=domain)
        entity.user_id = user.id
        db.session.add(entity)
        db.session.commit()

    @classmethod
    def get_domains_for_user(cls, user):
        if not user:
            return []
        full_user = cls.get_user(user['authority'], user['sub'])
        return [item.domain for item in full_user.domains]

    @classmethod
    def create_new_user(cls, authority, sub):
        info = cls(authority=authority, sub=sub, updated_at = datetime.datetime.utcnow())
        db.session.add(info)
        db.session.commit()

    @classmethod
    def check_user_exists(cls, authority, sub):
        return db.session.query(cls)\
                   .filter(cls.authority == authority)\
                   .filter(cls.sub == sub).count() > 0
    
    @classmethod
    def get_user(cls, authority, sub):
        if not cls.check_user_exists(authority, sub):
            return None
        return db.session.query(cls) \
            .filter(cls.authority == authority) \
            .filter(cls.sub == sub).first()

    @classmethod
    def get_claims_data_for_user(cls, user, claims, scope):
        if not user:
            return {}
        data = json.loads(user.claims)
        expanded = list()
        for claim in claims:
            expanded.append(claim)
        for value in scope:
            if value in cls.SCOPE_CLAIMS.keys():
                expanded += cls.SCOPE_CLAIMS[value]
        return {claim: data[claim] for claim in expanded if claim in data}

    @classmethod
    def get_claims_for_user(cls, user):
        if not user:
            return {}
        full_user = cls.get_user(user['authority'], user['sub'])
        if not full_user:
            return {}
        return json.loads(full_user.claims)

    @classmethod
    def update_claim_for_user(cls, user, claim, value):
        if not user:
            return
        full_user = cls.get_user(user['authority'], user['sub'])
        if not full_user:
            return {}
        data = json.loads(full_user.claims)
        if StandardClaim.is_standard_json_claim_field(claim):
            prefix = claim.split('_')[0]
            field = claim[len(prefix) + 1:]
            if not prefix in data:
                data[prefix] = {}
            data[prefix][field] = value
        elif claim in data and not value and not StandardClaim._is_named_standard_claim(claim):
            del data[claim]
        else:
            data[claim] = value
        full_user.claims = json.dumps(data)
        full_user.updated_at = datetime.datetime.utcnow()
        db.session.add(full_user)
        db.session.commit()


class Domain(db.Model):
    __tablename__ = 'domain'
    domain = db.Column(db.Text, nullable=False, primary_key=True)
    user_id = db.Column(db.Text, db.ForeignKey('user.id'))


class StandardClaim(db.Model):
    __tablename__ = 'standard_claims'
    claim = db.Column(db.Text, nullable=False, primary_key=True)
    category = db.Column(db.Text, nullable=False)
    type = db.Column(db.Text, nullable=False)
    fields = db.Column(db.Text)

    @classmethod
    def get_standard_claims(cls):
        return db.session.query(cls).all()

    @classmethod
    def get_standard_claims_of_type(cls, type):
        return db.session.query(cls).filter(cls.type == type).all()

    @classmethod
    def get_claim(cls, claim):
        return db.session.query(cls).filter(cls.claim == claim).first()

    @classmethod
    def is_standard_json_claim_field(cls, claim):
        if '_' not in claim:
            return False
        prefix = claim.split('_')[0]
        if not cls._is_named_standard_claim(prefix):
            return False
        standard_claim = cls.get_claim(prefix)
        if not standard_claim:
            return False
        field = claim[len(prefix) + 1:]
        return field in standard_claim.fields.split(',')

    @classmethod
    def _is_named_standard_claim(cls, claim):
        return db.session.query(cls).filter(cls.claim == claim).count() > 0

    @classmethod
    def is_standard_claim(cls, claim):
        return cls.is_standard_json_claim_field(claim) or cls._is_named_standard_claim(claim)

    @classmethod
    def validate_standard_claim(cls, claim, value):
        if cls.is_standard_json_claim_field(claim):
            return True, 'string'
        standard_claim = db.session.query(cls).filter(cls.claim == claim).first()
        if not standard_claim:
            return False, None
        if standard_claim.type == 'boolean':
            return value in ['True', 'False'], 'boolean'
        if standard_claim.type == 'date':
            try:
                datetime.datetime.strptime(value, '%Y-%m-%d')
                return True, 'date'
            except ValueError:
                return not value, standard_claim.type
        if standard_claim.type == 'email':
            if value and not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", value):
                return False, 'email'
            return True, 'email'
        if standard_claim.type == 'phone':
            if value and not re.match(r"^[0-9\+\s\(\)]*$", value):
                return False, 'phone'
            return True, 'phone'
        if standard_claim.type == 'timezone':
            return not value or value in pytz.all_timezones, 'timezone'
        if standard_claim.type == 'locale':
            return not value or value.lower() in locale.locale_alias.keys(), 'locale'
        if standard_claim.type == 'json':
            try:
                data = json.loads(value)
                fields = standard_claim.fields.split(',')
                if len(fields) != len(data.keys()):
                    raise ValueError
                for key in fields:
                    if key not in data:
                        raise ValueError
                return True, standard_claim.type
            except ValueError:
                return False, standard_claim.type
        return True, standard_claim.type

    @classmethod
    def cast_to_type_standard_claim(cls, claim, value):
        if cls.is_standard_json_claim_field(claim):
            return value
        standard_claim = db.session.query(cls).filter(cls.claim == claim).first()
        if not standard_claim:
            return None
        if standard_claim.type == 'boolean':
            return True if value == 'True' else False
        if standard_claim.type == 'json':
            try:
                return json.loads(value)
            except ValueError:
                return None
        return value


class Registration(db.Model):
    __tablename__ = 'registration'

    domain = db.Column(db.Text, nullable=False, primary_key=True)
    challenge_url = db.Column(db.Text, nullable=True)
    token = db.Column(db.Text, nullable=True)
    email = db.Column(db.Text, nullable=True)

    @classmethod
    def create_registration(cls, domain, email=None):
        user_registration = cls.get_domain_registration(domain)
        if not user_registration:
            user_registration = Registration(domain=domain, email=email)
        else:
            user_registration.email = email
            user_registration.token = None
            user_registration.challenge_url = None
        db.session.add(user_registration)
        db.session.commit()

    @classmethod
    def new_challenge(cls, domain, token, challenge_url):
        existing = db.session.query(cls).filter(cls.domain == domain).first()
        existing.token = token
        existing.challenge_url = challenge_url
        db.session.add(existing)
        db.session.commit()

    @classmethod
    def get_domain_registration(cls, domain):
        existing = db.session.query(cls).filter(cls.domain == domain).first()
        return existing

    @classmethod
    def finalize_challenge(cls, domain):
        existing = db.session.query(cls).filter(cls.domain == domain).first()
        existing.challenge_url = None
        existing.token = None
        db.session.add(existing)
        db.session.commit()

    @classmethod
    def complete_registration(cls, domain):
        existing = db.session.query(cls).filter(cls.domain == domain).first()
        db.session.delete(existing)
        db.session.commit()


class NotRegisteredError(Exception):
    pass


class IdentityAuthority(db.Model):
    __tablename__ = 'identity_authority'
    host = db.Column(db.Text, nullable=False, primary_key=True)
    client_configuration = db.Column(db.Text, nullable=True)
    agent_configuration = db.Column(db.Text, nullable=True)

    @classmethod
    def get_local_identity_authority_client_config(cls, identity_authority):
        ia = db.session.query(cls) \
            .filter(cls.host == identity_authority) \
            .first()
        if ia is None:
            raise NotRegisteredError()
        if ia.client_configuration is None:
            raise NotRegisteredError()
        return json.loads(ia.client_configuration)

    @classmethod
    def get_local_identity_authority_agent_config(cls, identity_authority):
        ia = db.session.query(cls) \
            .filter(cls.host == identity_authority) \
            .first()
        if ia is None:
            raise NotRegisteredError()
        if ia.agent_configuration is None:
            raise NotRegisteredError()
        return json.loads(ia.agent_configuration)

    @classmethod
    def update_identity_authority_client_config(cls, identity_authority, client_configuration):
        ia = db.session.query(cls) \
            .filter(cls.host == identity_authority) \
            .first()
        if not ia:
            ia = cls(host=identity_authority)
        ia.client_configuration = client_configuration
        db.session.add(ia)
        db.session.commit()

    @classmethod
    def update_identity_authority_agent_config(cls, identity_authority, agent_configuration):
        ia = db.session.query(cls) \
            .filter(cls.host == identity_authority) \
            .first()
        if not ia:
            ia = cls(host=identity_authority)
        ia.agent_configuration=agent_configuration
        db.session.add(ia)
        db.session.commit()


class AccessLog(db.Model):
    __tablename__ = 'access_log'
    ts = db.Column(db.DateTime(timezone=True), server_default=db.func.now(), primary_key=True)
    subject = db.Column(db.Text, nullable=False, primary_key=True)
    domain = db.Column(db.Text, nullable=False, primary_key=True)
    issuer = db.Column(db.Text, nullable=False, primary_key=True)
    claims = db.Column(db.Text, nullable=False, primary_key=True)
    client = db.Column(db.Text, nullable=True)

    @classmethod
    def get_for_user(cls, user, page_size, page):
        result = []
        logs = db.session.query(cls) \
            .filter(cls.subject == user['sub']) \
            .filter(cls.issuer == user['authority']) \
            .order_by(db.desc(cls.ts)) \
            .offset(page * page_size) \
            .limit(page_size) \
            .all()
        for log in logs:
            claims = log.claims.split(' ')
            result.append({
                'domain': log.domain,
                'timestamp': log.ts,
                'issuer': log.issuer,
                'claims': ', '.join(claims),
                'subject': log.subject,
                'client': log.client if log.client else ''
            })
        return result

    @classmethod
    def write(cls, domain, subject, issuer, claims, client):
        log = cls(domain=domain, subject=subject, issuer=issuer, claims=claims, client=client)
        db.session.add(log)
        db.session.commit()


class AuthenticationLog(db.Model):
    __tablename__ = 'authentication_log'
    uuid = db.Column(db.Text, nullable=False, primary_key=True)
    auth_time = db.Column(db.Integer, nullable=False)
    subject = db.Column(db.Text, nullable=False)
    identifier = db.Column(db.Text, nullable=False)
    issuer = db.Column(db.Text, nullable=False)
    client_id = db.Column(db.Text, nullable=False)
    success = db.Column(db.Boolean)

    @classmethod
    def get_for_user(cls, user, page_size, page):
        logs = db.session.query(cls) \
            .filter(cls.subject == user['sub']) \
            .filter(cls.issuer == user['authority']) \
            .order_by(db.desc(cls.auth_time)) \
            .offset(page * page_size) \
            .limit(page_size) \
            .all()
        result = []
        for log in logs:
            result.append({
                'auth_time': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(log.auth_time)),
                'uuid': log.uuid,
                'subject': log.subject,
                'issuer': log.issuer,
                'client_id': log.client_id,
                'success': log.success,
                'identifier': log.identifier
            })
        return result

    @classmethod
    def write(cls, uuid, auth_time, subject, identifier, issuer, client_id, success):
        log = cls(uuid=uuid, auth_time=auth_time, subject=subject, identifier=identifier, issuer=issuer,
                  client_id=client_id, success=success)
        db.session.add(log)
        db.session.commit()
