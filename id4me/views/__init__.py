from .auth import *
from .claims import *
from .logs import *
from .registration import *


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def server_error(e):
    return render_template('500.html', error=e), 500


@app.errorhandler(401)
def server_error(e):
    return render_template('401.html', error=e), 401
