from flask import render_template, session, request, redirect, url_for, jsonify
from urllib.parse import urlencode
from werkzeug.exceptions import Unauthorized

from id4me import app
from id4me.mail import send_magic
from id4me.aim import (
    check_existing_challenge, check_existing_registration, check_dns_for_existing_acme_record,
    request_dns_challenge, get_openid_record, get_dns_challenge_record, confirm_dns_challenge,
)
from id4me.domain_connect import get_domain_connect_template_redirect_url


@app.route('/register', methods=['GET', 'POST'])
def register():
    try:
        domain = session['domain']
        if check_existing_registration(domain):
            return redirect(url_for('challenge'))
        else:
            session.clear()
    except KeyError:
        pass
    if request.method == 'GET':
        return render_template('register.html', domain_hint=request.args.get("domain_hint", ""))
    domain = request.form['domain']
    email = request.form.get('email', None)
    existing = check_existing_registration(request.form['domain'])
    session['domain'] = request.form['domain']
    if existing:
        return redirect(url_for('challenge'))
    request_dns_challenge(domain, email)
    return redirect(url_for('challenge'))


@app.route('/challenge', methods=['GET'])
def challenge():
    try:
        domain = session['domain']
        if not check_existing_registration(domain):
            session.clear()
            raise KeyError
    except KeyError:
        return redirect(url_for('register'))
    existing = check_existing_registration(domain)
    if not existing:
        return redirect(url_for('register'))
    started = check_existing_challenge(domain)
    if not started:
        request_dns_challenge(domain, email=None)
    return render_template(
        template_name_or_list='challenge.html',
        records=[get_openid_record(), get_dns_challenge_record(domain)],
        domain=domain
    )


@app.route('/domain-connect-challenge', methods=['GET'])
def domain_connect_challenge():
    try:
        domain = session['domain']
        redirect_url, error = get_domain_connect_template_redirect_url(domain)
        if redirect_url:
            print("Redirecting to {}".format(redirect_url))
            return redirect(redirect_url)
    except KeyError:
        pass
    return redirect(url_for('register'))


@app.route('/challenge/status', methods=['POST'])
def challenge_status():
    try:
        domain = session['domain']
    except KeyError:
        return jsonify({
            'status': 302,
            'message': 'No domain found. Will redirect to registration.',
            'redirect': url_for('register')
        })
    existing = check_existing_registration(domain)
    if not existing:
        session.clear()
        return jsonify({
            'status': 302,
            'message': 'No registration found. Will redirect to registration.',
            'redirect': url_for('register')
        })
    try:
        email, magic = confirm_dns_challenge(domain)
    except Unauthorized:
        return jsonify({
            'status': 400,
            'message': 'Please complete ACME challenge.'
        })
    if not magic:
        return jsonify({
            'status': 302,
            'message': 'ACME challenge expired. Will generate another challenge.',
            'redirect': url_for('challenge')
        })
    if not email:
        return jsonify({
            'status': 302,
            'message': 'Challenge completed. Will redirect to account creation.',
            'redirect': magic + '&' + urlencode({'redirect_uri': app.config['SERVER_URL'] + url_for('login')})
        })
    try:
        send_magic(email, domain, magic + '&' + urlencode({
            'redirect_uri': app.config['SERVER_URL'] + url_for('login') + '?domain=' + domain
        }))
        return jsonify({
            'status': 201,
            'message': 'The link to setup password was sent to the desired email address ({}). If you start another '
                       'registration process for the same (sub)domain, the link will be invalidated.'.format(email)
        })
    except:
        return jsonify({
            'status': 422,
            'message': 'Sending the link to setup password to the desired email address ({}) has failed. Here is the '
                       'link we tried to send: <br> <b>{}</b>'.format(
                email,
                magic + '&' + urlencode({
                    'redirect_uri': app.config['SERVER_URL'] + url_for('login') + '?domain=' + domain
                })
            )
        })


@app.route('/check-acme-dns', methods=['POST'])
def check_acme_dns():
    try:
        domain = session['domain']
    except KeyError:
        return jsonify({
            'status': 302,
            'message': 'No domain found. Will redirect to registration.',
            'redirect': url_for('register')
        })
    existing_record = check_dns_for_existing_acme_record(domain)
    if existing_record:
        return jsonify({
            'status': 400,
            'message': 'An ACME challenge record found. Please remove it before adding the new one.'
        })
    return jsonify({
        'status': 404,
        'message': 'No ACME challenge record found.'
    })


@app.route('/check-domain-connect-dns', methods=['POST'])
def check_domain_connect_dns():
    try:
        domain = session['domain']
    except KeyError:
        return jsonify({
            'status': 302,
            'message': 'No domain found. Will redirect to registration.',
            'redirect': url_for('register')
        })
    redirect_url, error = get_domain_connect_template_redirect_url(domain)
    if redirect_url:
        return jsonify({
            'status': 200,
            'message': 'Domain Connect found for domain.'
        })
    return jsonify({
        'status': 400,
        'message': 'Domain Connect not found for domain.'
    })
