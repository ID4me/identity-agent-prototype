import calendar
import datetime

import jwt
from flask import (
    make_response, render_template, request, url_for, Response,
    jsonify, session, flash
)
from werkzeug.exceptions import Unauthorized

from id4me import app
from id4me.identity_authority import decode_validation_token, require_openid_delegation
from id4me.models import User, AccessLog, StandardClaim


@app.route('/', methods=['GET', 'POST'])
def index():
    try:
        user = session['user']
        valid = session['valid']
    except KeyError:
        return render_template('login.html')

    data = User.get_claims_for_user(user)
    custom_claims = []
    claims = {item.claim: '' if item.type != 'json' else {} for item in StandardClaim.get_standard_claims()}
    for claim in claims.keys():
        if claim in data:
            claims[claim] = data[claim]
            del data[claim]
    for custom_claim in data.keys():
        custom_claims.append({
            'claim': custom_claim,
            'value': data[custom_claim]
        })

    if request.method == 'POST':
        if request.form.keys():
            form_valid = True
            for claim in request.form.keys():
                value = request.form.get(claim, '')
                claims[claim] = value
            for claim in request.form.keys():
                if StandardClaim.is_standard_claim(claim):
                    value = request.form.get(claim, '')
                    valid, claim_type = StandardClaim.validate_standard_claim(claim, value)
                    if not valid:
                        flash('{} must be a valid {}.'.format(claim.capitalize(), claim_type), 'error')
                        form_valid = False
            if form_valid:
                boolean_claims = [item.claim for item in StandardClaim.get_standard_claims_of_type('boolean')]
                for boolean_claim in boolean_claims:
                    if boolean_claim not in request.form.keys():
                        User.update_claim_for_user(user, boolean_claim, False)
                        claims[boolean_claim] = False
                for claim in request.form.keys():
                    if StandardClaim.is_standard_claim(claim):
                        value = request.form.get(claim, '')
                        User.update_claim_for_user(user, claim, StandardClaim.cast_to_type_standard_claim(claim, value))
                        if StandardClaim.is_standard_json_claim_field(claim):
                            prefix = claim.split('_')[0]
                            field = claim[len(prefix) + 1:]
                            claims[prefix][field] = value
                        else:
                            claims[claim] = StandardClaim.cast_to_type_standard_claim(claim, value)
                    else:
                        User.update_claim_for_user(user, claim, request.form.get(claim, ''))
                        deleted = not request.form.get(claim, '')
                        if deleted:
                            custom_claims = [item for item in custom_claims if item['claim'] != claim]
                        else:
                            new = True
                            for item in custom_claims:
                                if item['claim'] == claim:
                                    item['value'] = request.form.get(claim, '')
                                    new = False
                            if new:
                                custom_claims.append({
                                    'claim': claim,
                                    'value': request.form.get(claim, '')
                            })
                flash('Data updated successfully.', 'success')
        else:
            flash('No data received', 'error')

    response = make_response(
        render_template(
            'claims.html',
            data=data,
            domains=User.get_domains_for_user(user),
            claims=claims,
            custom_claims=custom_claims
        )
    )
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Expires'] = datetime.datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
    return response


@app.route('/userinfo')
def user_info():
    auth = request.headers.get('Authorization')
    if auth is None:
        print('Authorization header missing')
        raise Unauthorized('Authorization header missing')
    try:
        method, token = auth.split(' ')
    except ValueError:
        print('Failed to parse authorization header')
        raise Unauthorized('Failed to parse authorization header')
    if method != 'Bearer':
        print('Expected method "Bearer" and found "{}"'.format(method))
        raise Unauthorized('Expected method "Bearer" and found "{}"'.format(method))
    payload = decode_validation_token(token)
    domain = payload['id4me.identifier'] if 'id4me.identifier' in payload else None
    subject = payload['sub']
    # require_openid_delegation(domain, payload['iss'])
    try:
        authorized_claims = payload['clm']
        scope = payload['scope']
    except KeyError:
        authorized_claims = []
        scope = []
    user = User.get_user(payload['iss'], subject)
    if user is None:
        return jsonify(
            {
                'error': 'identity_unknown',
                'error_description': 'no identity for requested iss/sub pair ({}. {})'.format(payload['iss'], subject)
            }), 404
    claims = User.get_claims_data_for_user(user, authorized_claims, scope)
    cstring = ' '.join(sorted(claims.keys()))
    print('Reported claims: {}'.format(cstring))
    AccessLog.write(domain, subject, payload['iss'], cstring, payload['client_id'])
    if request.accept_mimetypes['application/jwt'] >= request.accept_mimetypes['application/json']:
        print('Returning result as JWT')
        updated_at = user.updated_at if user else datetime.datetime.utcnow()
        payload = {
            'aud': payload['client_id'],
            'iss': app.config['SERVER_URL'],
            'sub': subject,
            'id4me.identifier': domain,
            'iat': datetime.datetime.utcnow(),
            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=5),
            'nbf': datetime.datetime.utcnow(),
            'updated_at': calendar.timegm(updated_at.utctimetuple()) if updated_at else None
        }
        payload.update(claims)
        return Response(jwt.encode(payload, app.private_key, 'RS256', headers={'kid': 'keyId'}), content_type='application/jwt')
    else:
        print('Returning result as JSON')
        return jsonify(claims=claims)


@app.route('/.well-known/openid-configuration')
def openid_config():
    server = app.config['SERVER_URL']
    result = {
        "issuer": server,
        "jwks_uri": "{}{}".format(server, url_for('jwks')),
        "userinfo_endpoint": "{}{}".format(server, url_for('user_info')),
        "claims_supported": [item.claim for item in StandardClaim.get_standard_claims()],
        "callback_endpoint": "{}{}".format(server, url_for('auth_callback', uuid='')),
    }
    return jsonify(result)


@app.route("/jwks.json")
def jwks():
    pubkey = {
        "kty": "RSA",
        "use": "sig",
        "kid": "keyId",
        "e": app.public_key_e.decode('UTF-8'),
        "n": app.public_key_n.decode('UTF-8'),
    }
    return jsonify(keys=[pubkey])
