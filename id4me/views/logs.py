import datetime

from flask import request, render_template, make_response, session

from id4me import app
from id4me.identity_authority import get_identity_authority_configuration
from id4me.models import AccessLog, AuthenticationLog
from id4me.network import get_json


def get_client_name(authority, client_id):
    iau_configuration = get_identity_authority_configuration(authority)
    clients_url = iau_configuration['registration_endpoint']
    if clients_url[-1] != '/':
        clients_url += '/'
    clients_url += client_id
    try:
        client_data = get_json(clients_url)
        return client_data['client_name']
    except:
        return ''


@app.route('/access_log')
def access_log():
    try:
        user = session['user']
        valid = session['valid']
    except KeyError:
        return render_template('login.html')

    page = request.args.get('page', '0')
    try:
        page = int(page)
        if page > 100:
            page = 100
        if page < 0:
            page = 0
    except ValueError:
        page = 0
    result = AccessLog.get_for_user(session['user'], 10, page)
    distinct_clients = {log['client'] for log in result}
    client_names = {client: get_client_name(user['authority'], client) for client in distinct_clients}
    response = make_response(render_template('access_log.html', log=result, client_names=client_names, page=page))
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Expires'] = datetime.datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
    return response


@app.route('/authentication_log')
def auth_log():
    try:
        user = session['user']
        valid = session['valid']
    except KeyError:
        return render_template('login.html')
    page = request.args.get('page', '0')
    try:
        page = int(page)
        if page > 100:
            page = 100
        if page < 0:
            page = 0
    except ValueError:
        page = 0
    result = AuthenticationLog.get_for_user(session['user'], 10, page)
    distinct_clients = {log['client_id'] for log in result}
    client_names = {client: get_client_name(user['authority'], client) for client in distinct_clients}
    response = make_response(render_template('authentication_log.html', client_names=client_names, log=result, page=page))
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Expires'] = datetime.datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
    return response
