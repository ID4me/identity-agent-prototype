import json
import re
import sqlite3
import urllib

from flask import request, session, url_for, redirect, render_template
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import BadRequest, Unauthorized

from id4me import app
from id4me.identity_authority import (
    get_identity_authority, get_identity_authority_configuration, register_identity_authority,
    decode_access_token, decode_validation_token
)
from id4me.models import IdentityAuthority, NotRegisteredError, AuthenticationLog, User
from id4me.network import post_data


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        domain = request.form.get('domain', '')
    elif 'domain' in session:
        domain = session['domain']
    else:
        domain = request.args.get('domain', '')
    if not domain:
        return redirect(url_for('index'))
    try:
        identity_authority = get_identity_authority(domain)
    except Unauthorized:
        return render_template(template_name_or_list='login.html', wrong_dns_domain=domain)
    print('identity_authority = {}'.format(identity_authority))
    iau_well_known = get_identity_authority_configuration(identity_authority)
    try:
        identity_authority_config = IdentityAuthority.get_local_identity_authority_client_config(identity_authority)
    except NotRegisteredError:
        # try to register with unknown iau
        identity_authority_config = register_identity_authority(identity_authority, iau_well_known)
    session['domain'] = domain
    session['iau'] = identity_authority
    session['client_id'] = identity_authority_config['client_id']
    session['issuer'] = iau_well_known['issuer']
    session['jwks_uri'] = iau_well_known['jwks_uri']
    session['token_endpoint'] = iau_well_known['token_endpoint']
    endpoint = '{}{}'.format(app.config['SERVER_URL'], url_for('validate'))
    destination = '{}?scope=openid&response_type=code&client_id={}&redirect_uri={}' \
                  '&state={}&login_hint={}'.format(
        iau_well_known['authorization_endpoint'],
        urllib.parse.quote(identity_authority_config['client_id']),
        urllib.parse.quote(endpoint),
        urllib.parse.quote('asdf'),
        urllib.parse.quote(domain)
    )
    print('destination = {}'.format(destination))
    return redirect(destination)


@app.route('/validate')
def validate():
    code = request.args['code']
    state = request.args['state']
    data = 'grant_type=authorization_code&code={}&redirect_uri={}{}'.format(
        code, app.config['SERVER_URL'], url_for('validate'))
    registration = IdentityAuthority.get_local_identity_authority_client_config(session['iau'])
    try:
        response = json.loads(
            post_data(
                session['token_endpoint'],
                data,
                basic_auth=(registration['client_id'], registration['client_secret'])
            )
        )
    except Exception:
        raise Unauthorized('Failed to get authorization token from {}'.format(session['iau']))
    payload = decode_access_token(response['access_token'], session['issuer'], session['client_id'], session['domain'])
    if not User.check_user_exists(payload['iss'], payload['sub']):
        User.create_new_user(payload['iss'], payload['sub'])
    User.register_domain(payload['iss'], payload['sub'], session['domain'])
    session['user'] = User.get_user(payload['iss'], payload['sub']).get_dict()
    session['valid'] = True
    session['iau'] = app.config.get('IAU')
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


@app.route('/callback/authentication/<uuid>', methods=['PUT'])
def auth_callback(uuid):
    uuid = uuid.lower()
    m = re.match('[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}$', uuid)
    if m is None:
        print('Malformed UUID {}'.format(uuid))
        raise BadRequest('Malformed UUID')
    payload = decode_validation_token(request.data, is_id_token=False)
    try:
        issuer = payload['iss']
        subject = payload['sub']
        identifier = payload['identifier']
        client_id = payload['client_id']
        success = payload['success']
        auth_time = payload['auth_time']
    except KeyError as ex:
        print('Claim missing: {} ({})'.format(ex, uuid))
        raise BadRequest('Claim missing: {}'.format(ex))
    if not isinstance(success, bool):
        print('Claim "success" is not boolean ({})'.format(uuid))
        raise BadRequest('Claim "success" is not boolean')
    try:
        auth_time = int(auth_time)
    except ValueError:
        print('Claim "auth_time" is not a valid number ({})'.format(uuid))
        raise BadRequest('Claim "auth_time" is not a valid number')
    if auth_time <= 0:
        print('Claim "auth_time" must be greater than zero ({})'.format(uuid))
        raise BadRequest('Claim "auth_time" must be greater than zero')
    try:
        AuthenticationLog.write(uuid, auth_time, subject, identifier, issuer, client_id, success)
        return ''
    except sqlite3.IntegrityError or IntegrityError as error:
        print(error)
        raise BadRequest("Could not save authentication log")
