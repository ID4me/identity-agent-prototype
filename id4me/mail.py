import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from flask import render_template

from id4me import app


def send_magic(email, domain, magic_link):
    mail = smtplib.SMTP(app.config.get("MAIL_SERVER"), app.config.get("MAIL_PORT"))
    mail.ehlo()
    mail.starttls()
    if "MAIL_USERNAME" in app.config and app.config.get("MAIL_USERNAME"):
        mail.login(app.config.get("MAIL_USERNAME"), app.config.get("MAIL_PASSWORD"))
    with app.app_context():
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Confirm your ID4me registration"
        msg['From'] = "reg@identityagent.de"
        msg['To'] = email

        text = render_template('magic-mail/email.txt', domain=domain, magic=magic_link, iau=app.config.get('IAU'), iag=app.config.get('SERVER_URL'))
        html = render_template('magic-mail/email-inline.html', domain=domain, magic=magic_link, iau=app.config.get('IAU'), iag=app.config.get('SERVER_URL'))

        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        msg.attach(part1)
        msg.attach(part2)

        mail.sendmail("reg@identityagent.de", email, msg.as_string())
        mail.quit()
