![id4me](http://id4me.org/wp-content/uploads/2018/03/ID4me_logo_2c_pos_rgb.png)

ID4me Open Prototype
--------------------

In order to help interested parties better understand how the **ID4me** protocol works and give the developers a 
starting point for their implementation, we have built an **Open Prototype** which includes the core functionalities of 
an agent. The **Open Prototype** was built as a proof of concept and it is not intended to be used in production.   

The agent works together with the **AUTHORITY** implementation provided by **DENIC** (https://id.denic.de/). 


Core functionalities
--------------------

The Open Prototype implementation has full **AGENT** functionalities as they are presented in 
[ID4me Technical Documentation](http://id4me.org/files/ID4me_Technical_Overview_v1.2.2.pdf):

* new identity registration and domain proof of ownership via **ACME** (*user has to manually add the DNS records) 
* claims storage
* user info retrieval
* claims management 
* access log
* authorization log 
 

Beside the core functionalities of an **AGENT** the open prototype also uses **ID4me** as a login method for the 
identity management, this way providing also a full implementation of a **RELYING PARTY**.   

Technical details
--------------------

The **Open Prototype** is written in **Python** and is using an embedded storage provided by **SQLite**. Our main concern
was to build something simple to understand and easy to get it running on a local machine. But keep in mind that when the 
application is running on a local environment some of the functionalities will not be available as the authority will 
not be able to send requests to the agent.

How to run
--------------------

* Install Docker (https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* Build image locally
    ```
    docker build . -t id4me
    ```
* Create a new `id.db` sqlite database and apply `schema.sql`
    ```
    sqlite3 id.db < schema.sql
    chown www-data:www-data id.db
    ```

* Create your system-configuration
    ```
    # Start from example
    cp ./system-configuration.example ./system-configuration
    
    # Generate a RSA key - needed for signing JWT
    openssl genrsa -out ./system-configuration/id4me/private-key.pem 2048
    
    # Python settings
    vim ./system-configuration/id4me/main.cfg
    
    # Nginx settings
    vim ./system-configuration/nginx/id4me
    ```

* Place your own SSL certificate `.cer` and `.key` in `system-configuration`
    ```
    cp [local-path-to-certificate]/[certificate].cer ./system-configuration/nginx/ssl_certificate.cer
    cp [local-path-to-certificate]/[certificate].key ./system-configuration/nginx/ssl_certificate.key
    ```

* Run new container
    ```
    docker run --name id4me -p [IP]:[EXPOSED_PORT]:443 -d -v [local-path-to-db]/id.db:/usr/local/lib/id4me/id.db -v [local-path-to-system-config]/system-configuration:/etc/system-configuration id4me
    ```
* Check https://[IP]:[EXPOSED_PORT]


How to update existing app
-------------------------

* Rebuild docker image (only if repository was updated)
    ```
    docker build . -t id4me
    ```
* If necessary, update your system configuration
    ```
    # Python settings
    vim ./system-configuration/id4me/main.cfg
    
    # Nginx settings
    vim ./system-configuration/nginx/id4me
    ```
* Restart container
    ```
    # Stop and remove existing instance
    docker stop id4me && docker rm id4me
    
    # Start id4me
    docker run --name id4me -p [IP]:[EXPOSED_PORT]:443 -d -v [local-path-to-db]/id.db:/usr/local/lib/id4me/id.db -v [local-path-to-system-config]/system-configuration:/etc/system-configuration id4me
    ```
* If you want to reset app's account registered at an authority:
    ```
    sqlite3 id.db 'DELETE FROM identity_authority WHERE host = "[host]"';
    ```
    * Example:
    ```
    sqlite3 id.db 'DELETE FROM identity_authority WHERE host = "id.denic.de"';
    ```
    **WARNING**: if you do this, you must also update your `private-key.pem` & `main.cfg` with a new agent name.