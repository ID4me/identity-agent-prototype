CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  sub    TEXT NOT NULL,
  authority TEXT NOT NULL,
  claims TEXT NOT NULL,
  updated_at TIMESTAMP
);

CREATE TABLE domain (
  domain  TEXT NOT NULL,
  user_id TEXT NOT NULL,
  PRIMARY KEY (domain),
  FOREIGN KEY(user_id) REFERENCES user(id)
);

CREATE TABLE standard_claims (
  claim TEXT NOT NULL,
  type  TEXT NOT NULL,
  category TEXT NOT NULL,
  fields TEXT,
  PRIMARY KEY (claim)
);

INSERT INTO standard_claims (claim, category, type, fields) values
  ('name', 'personal_data', 'string', ''),
  ('given_name', 'personal_data', 'string', ''),
  ('family_name', 'personal_data', 'string', ''),
  ('middle_name', 'personal_data', 'string', ''),
  ('nickname', 'personal_data', 'string', ''),
  ('preferred_username', 'main_contact', 'string', ''),
  ('profile', 'personal_data', 'string', ''),
  ('picture', 'personal_data', 'string', ''),
  ('website', 'personal_data', 'string', ''),
  ('email', 'main_contact', 'email', ''),
  ('email_verified', 'main_contact', 'boolean', ''),
  ('gender', 'personal_data', 'string', ''),
  ('birthdate', 'personal_data', 'date', ''),
  ('zoneinfo', 'personal_data', 'timezone', ''),
  ('locale', 'personal_data', 'locale', ''),
  ('phone_number', 'main_contact', 'phone', ''),
  ('phone_number_verified', 'main_contact', 'boolean', ''),
  ('address', 'address', 'json', 'formatted,street_address,locality,region,postal_code,country')
;

CREATE TABLE identity_authority (
  host          TEXT NOT NULL,
  agent_configuration TEXT,
  client_configuration TEXT,
  PRIMARY KEY (host)
);

CREATE TABLE registration (
  domain        TEXT NOT NULL,
  challenge_url TEXT,
  token         TEXT,
  email         TEXT,
  PRIMARY KEY (domain)
);

CREATE TABLE access_log (
  ts      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  domain  TEXT NOT NULL,
  subject TEXT NOT NULL,
  issuer  TEXT NOT NULL,
  claims  TEXT NOT NULL,
  client  TEXT,
  PRIMARY KEY (ts, subject, issuer, claims)
);

CREATE TABLE authentication_log (
  uuid      TEXT       NOT NULL,
  auth_time INTEGER    NOT NULL,
  subject   TEXT       NOT NULL,
  issuer    TEXT       NOT NULL,
  identifier TEXT      NOT NULL,
  client_id TEXT       NOT NULL,
  success   INTEGER(1) NOT NULL,
  PRIMARY KEY (uuid)
);
