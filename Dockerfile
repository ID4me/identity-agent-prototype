FROM python:3-slim-stretch

RUN apt-get update && apt-get install -y \
    autoconf \
    automake \
    autotools-dev \
    libev4 \
    libsigsegv2 \
    m4 \
    nginx-light \
    python3-crypto \
    python3-dev \
    python3-dnspython \
    python3-flask \
    python3-flask-sqlalchemy \
    python3-jwt \
    python3-sqlalchemy \
    python3-publicsuffix \
    python3-tz \
    sqlite3 \
    systemd \
    unbound \
    unbound-anchor \
    uwsgi \
    uwsgi-plugin-python3

VOLUME /etc/system-configuration

RUN mkdir -p /etc/nginx/ssl
COPY id4me /usr/local/lib/id4me
RUN ln -s /etc/system-configuration/nginx/id4me /etc/nginx/sites-enabled/id4me
RUN ln -s /etc/system-configuration/nginx/id4me-http /etc/nginx/sites-enabled/id4me-http
RUN rm /etc/nginx/sites-enabled/default

RUN cd /usr/lib/python2.7/ && \
    ln -s plat-x86_64-linux-gnu/_sysconfigdata_nd.py .
RUN ln -s /etc/system-configuration/id4me/uwsgi.ini /etc/uwsgi/apps-enabled/id4me.conf
RUN chown www-data:www-data -R /usr/local/lib/id4me/

WORKDIR /usr/local/lib/id4me/

VOLUME /usr/local/lib/id4me/id.db

ENV PYTHONPATH /usr/local/lib/

EXPOSE 443
CMD service nginx restart && uwsgi --ini /etc/system-configuration/id4me/uwsgi.ini
